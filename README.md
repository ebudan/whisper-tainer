# whisper-cli

A containerized OpenAI Whisper setup to avoid polluting devstations with old Python.
Requirements have been carefully selected to provide Whisper and its dependencies with library versions they can live with - at the time of this writing. 

## Prerequisites

Install [Task](https://taskfile.dev/).

Install [Podman](https://podman.io/) (or equivalent). 

## Usage

```
task build-image
task run
```

Outside the container, provide some audio:

```
ffmpeg -f alsa -ac 2 -i default -acodec libmp3lame -ab 320k workdir/recording.mp3
```

In the container, transcript:

```
whisper recording.mp3
```

Provide to the next model in the chain to get a summary. See `tools/prompt` for an example prelude for a `.srt` transcript.

```
cat recording.srt | wl-copy --type text/plain
...
```



